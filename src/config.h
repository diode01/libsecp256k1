#include "basic-config.h"

#ifndef _SECP256K1_CONFIG_
#define _SECP256K1_CONFIG_

// secp256k1 config
#define USE_NUM_NONE
#define USE_FIELD_10X26
#define USE_FIELD_INV_BUILTIN
#define USE_SCALAR_8X32
#define USE_SCALAR_INV_BUILTIN
#define NDEBUG

#endif