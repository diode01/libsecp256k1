LIBSECP256K1_SRC = middleware/third_party/libsecp256k1

#
# Get the list of C files
#

libsecp256k1_PATH = $(shell pwd)

C_FILES += ${LIBSECP256K1_SRC}/src/secp256k1.c

#################################################################################
#include path
CFLAGS += -I${SOURCE_DIR}/${LIBSECP256K1_SRC}
CFLAGS += -I${SOURCE_DIR}/${LIBSECP256K1_SRC}/include
CFLAGS += -I${SOURCE_DIR}/${LIBSECP256K1_SRC}/src/modules/recovery
